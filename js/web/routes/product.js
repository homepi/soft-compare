
// ## dependencies
var express = require('express');

var lib = require('../lib');

// ## config
var router = express.Router();
var log = lib.log.getLogger(__filename);

// ## export
var ns = {router: router};
module.exports = ns;

// ## func

// ## routes
router.use('/', function(req, res, next){
	var url = req.originalUrl.split("?")[0];
	
	if( req.method === 'GET' ){
		log.debug('%s %s - query = %j', req.method, url, req.query);
	}
	if( req.method === 'POST' ){
		log.debug('%s %s - params = %j | body = %j', 
			req.method, url, req.params, req.body);
	}
	
	return next();
});
router.get('/', async function(req, res){
	var result = await lib.dba.search(req.query);
	return res.json({ data: result });
});

router.post('/:id/inc', async function(req, res){
	log.debug('post.params = %j', req.params);
	var result = await lib.dba.search({id: Number(req.params.id) });
	var target = result[0];
	log.debug('post.found = %j', target);
	
	var toadd = (req.body.amount && req.body.amount > 0) ?
		req.body.amount : 1;
	target.STOCK += toadd;
	await lib.dba.update(target);
	
	return res.json(target);
});


