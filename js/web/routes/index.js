
// ## dep
var express = require('express');
var router = express.Router();

// ## expose
var ns = {router: router};
module.exports = ns;

// ## load
var path = require("path");
var fs = require("fs");
var normalizedPath = path.join(__dirname, "./");

fs.readdirSync(normalizedPath).forEach(function(file){
	var moduleName = file.replace('.js', '');
	ns[moduleName] = require("./" + file);
});

router.use('/api/products', ns.product.router);


