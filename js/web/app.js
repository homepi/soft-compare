
// ## dependencies
var express = require('express');

var lib = require('./lib');
var routes = require('./routes');

// ## config
var config = require('./config');

var server = express();
server.use(express.json());

var log = lib.log.getLogger(__filename);

// ## functions

// ## routing
server.get('/', function(req, res){
	return res.json({ok: true});
});
server.use(routes.router);

// ## flow
lib.dba.init();

log.info('server start - port = %s', config.PORT);
server.listen(config.PORT, function(){
	log.info('server started...');
});


