
// ## dependencies
var logLib = require('loglevel');

// ## config
var config = require('../config');
logLib.setLevel(config.LOG);

// ## export
var ns = {};
module.exports = ns;

// ## func
ns.reduceFileName = function(fileName){
	var cwd = process.cwd().replace(/\\/g, '/') + "/";
	fileName = fileName.replace(/\\/g, '/');
	return fileName.replace(cwd, '').replace('.js', '');
};

ns.getLogger = function(fileName){
	
	fileName = ns.reduceFileName(fileName);
	
	var print = function(level){
		return function(...msg){
			var date = new Date();
			var ts = date.toISOString();
			msg[0] = `${ts} :: ${fileName} >> ${msg[0]}`;
			return logLib[level](...msg);
		};
	};
	
	return {
		debug: print('debug'),
		info: print('info'),
		warn: print('warn'),
		error: print('error')
	};
};

