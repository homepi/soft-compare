
// ## expose
var ns = {};
module.exports = ns;

// ## load
var path = require("path");
var fs = require("fs");
var normalizedPath = path.join(__dirname, "./");

fs.readdirSync(normalizedPath).forEach(function(file){
	var moduleName = file.replace('.js', '');
	ns[moduleName] = require("./" + file);
});


