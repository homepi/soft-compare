
// ## dependencies
var fs = require('fs');
var csv = require('fast-csv');
var logHelper = require('./log');

// ## config
var csvFileName = '../../data/products.csv';
var dataTable = [];

var log = logHelper.getLogger(__filename);

// ## export
var ns = {};
module.exports = ns;

// ## func
ns.parseCSV = function(txt){
	
	var opts = {delimiter: ';', headers: true};
	var arr = [];
	
	return new Promise(function(resolve, reject){
		csv.parseString(txt, opts)
			.on('error', error => reject(error))
			.on('data', row => arr.push(row))
			.on('end', rowCount => resolve(arr));
	});
};

ns.init = async function(){
	var fileContent = fs.readFileSync(csvFileName, 'utf-8');
	var parsed = await ns.parseCSV(fileContent);
	
	var nums = ['ID', 'PRICE', 'STOCK'];
	dataTable = parsed.map(function(x){
		for(var n of nums){
			x[n] = Number(x[n]);
		}
		
		return x;
	});
	log.info('init - end - len = %s | keys = %j', 
		dataTable.length, Object.keys(dataTable[0]));
	return true;
};

ns.search = async function(query){
	
	var keyLen = Object.keys(query).length;
	var result = dataTable.filter(function(x){
		var m = [];
		for(var k in query){
			
			var low = x[k.toLowerCase()];
			var upp = x[k.toUpperCase()];
			
			var target = (low || upp);
			var criteria = query[k];
			
			if( target && typeof(target) === 'string' ){
				target = target.toLowerCase()
			}
			if( criteria && typeof(criteria) === 'string' ){
				criteria = criteria.toLowerCase()
			}
			
			var same = (criteria === target);
			m.push( same );
		}
		
		var good = m.filter(y => y);
		return good.length === keyLen;
	});
	return result;
};

ns.update = async function(obj){
	return true;
};


