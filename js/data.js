
// ## dep
var fs = require('fs');

// ## config
var filePath = './data/large-json.json';

// ## fund
var main = function(){
	
	console.log('*** nodejs - start ***');
	var txt = fs.readFileSync(filePath, 'utf-8');
	var json = JSON.parse(txt);
	
	console.log('len = %s | keys = %j', json.length, Object.keys(json[0]));
	var arr = [];
	json.forEach(function(x){
		var sum = x.index + x.age + x.friends.length;
		var prod = sum * 16782;
		var mod = prod % 15000;
		
		for(var i=0; i<mod; i++){
			arr.push({sum: sum, prod: prod, mod: mod, i: i, id: x._id});
		}
	});
	
	console.log('arr - %s', arr.length);
	console.log('*** nodejs - end ***');
	return 0;
};

// ## flow
ns.main();


