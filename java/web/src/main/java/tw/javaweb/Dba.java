package tw.javaweb;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.opencsv.bean.CsvToBeanBuilder;

public class Dba {
	
	String FILE_NAME = "../../data/products.csv";
	List<Product> DataHolder;
	private static final Logger log = LoggerFactory.getLogger(Dba.class);
	
	public Dba(){
		log.info("dba - constructor...");
	}
	
	public void Init() throws FileNotFoundException{
		var fileReader = new FileReader(FILE_NAME);
		var csvBuilder = new CsvToBeanBuilder<Product>(fileReader);
		
		var csvOpts = csvBuilder.withSeparator(';')
			.withSkipLines(1)
			.withIgnoreLeadingWhiteSpace(true)
			.withFilter(stringValues -> Arrays.stream(stringValues)
				.anyMatch(value -> value != null && value.length() > 0))
			.withType(Product.class);
		
		DataHolder = csvOpts.build().parse();
	}
	
	public List<Product> Search(Map<String, ?> query){
		
		var keyLen = query.size();
		log.info("Search - start - keyLen = {}", keyLen);
		
		var result = DataHolder.stream().filter((Product x) -> {
			var hit = new ArrayList<Boolean>();
			
			query.forEach((key, val) -> {
				var check = false;
				var objKey = key.toUpperCase();
				
				try {
					var objVal = x.getClass().getField(objKey).get(x);
					log.info("k = {} | q = {} | val = {}", key, val, objVal);
					if( objVal instanceof String ){
						check = ((String) val).equalsIgnoreCase((String) objVal);
					}
					if( objVal instanceof Integer ){
						check = ( (int)objVal == (int)val );
					}
					if( check ){
						hit.add(check);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});
			
			return hit.size() == keyLen;
		}).toList();
		
		return result;
	}
	
	public boolean Update(Product obj){
		return true;
	}
}


