package tw.javaweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SpringBootApplication
public class Application {
	
	
	static final Logger log = LoggerFactory.getLogger(Application.class);
	
	public static void main(String[] args){
		log.info("Started...");
		SpringApplication.run(Application.class, args);
	}
	
}


