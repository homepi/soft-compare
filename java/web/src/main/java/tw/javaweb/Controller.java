package tw.javaweb;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
	static final Logger log = LoggerFactory.getLogger(Controller.class);
	static Dba dba;
	
	public Controller() throws FileNotFoundException {
		dba = new Dba();
		dba.Init();
	}
	
	@RequestMapping( value="/", method=RequestMethod.GET, produces="application/json" )
	@ResponseBody
	public String hello(){
		log.info("GET / - start...");
		return "{\"test\": true}";
	}
	
	@RequestMapping(
		value = "/api/products", 
		method = RequestMethod.GET, 
		produces="application/json"
	) 
	@ResponseBody
	public Map<String, Object> search(@RequestParam(required=false) Map<String, String> qparams){
		log.info("GET /api/products - start - q = {}", qparams.toString());
		var data = dba.Search( qparams );
		var res = new HashMap<String, Object>();
		res.put("data", data);
		return res;
	}
	
	@RequestMapping(
		value = "/api/products/{id}/inc",
		method = RequestMethod.POST,
		produces="application/json"
	) 
	@ResponseBody
	public Product update(@PathVariable int id, @RequestBody Map<String, Object> body)
	{
		
		log.info("POST - start - id = {}", id);
		var query = new HashMap<String, Object>();
		query.put("id", id);
		
		var obj = dba.Search(query).get(0);
		log.info("POST - found - name = {} | stock = {}", obj.NAME, obj.STOCK);
		
		int toadd = 1;
		if( body.containsKey("amount") && body.get("amount") instanceof Integer ){
			toadd = (int) body.get("amount");
		}
		obj.STOCK += toadd;
		dba.Update(obj);
		
		return obj;
	}
}

