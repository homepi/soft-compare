import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.io.File;
import java.nio.file.Paths;
import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;

public class data {
	public static void main(String[] args) throws java.io.IOException
	{
        System.out.println("*** java - start ***");
		
		ObjectMapper mapper = new ObjectMapper();
		File file = Paths.get("../data/large-json.json").toFile();
		Map[] list = mapper.readValue(file, Map[].class);
		
		List<Object> arr = new ArrayList<Object>();
		
		for (Map x : list) 
		{ 
			ArrayList friends = (ArrayList)x.get("friends");
			int sum = (int)x.get("index") + (int)x.get("age") + friends.size();
			int prod = sum * 16782;
			int mod = prod % 15000;
			
			for(int i=0; i<mod; i++)
			{
				Map<String, Object> obj = new HashMap<String, Object>();
				
				obj.put("sum", sum);
				obj.put("prod", prod);
				obj.put("mod", mod);
				obj.put("i", i);
				obj.put("id", (String)x.get("id"));
				
				arr.add(obj);
			}
		}
		
		System.out.format("arr - %d \n", arr.size());
		System.out.println("*** java - end ***");
		
    }
}


