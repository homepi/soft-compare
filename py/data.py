
## dep
import json

## config
filePath = './data/large-json.json'

## fund
def main():
    print("*** py start ***")
    file = open(filePath)
    obj = json.load(file)
    file.close()
    
    print('len = {}'.format( str(len(obj)) ))
    arr = []
    
    for x in obj:
        s = x['index'] + x['age'] + len(x['friends'])
        prod = s * 16782;
        mod = prod % 15000;
        
        for i in range(mod):
            arr.append({"sum": s, "prod": prod, "mod": mod, "i": i, "id": x['_id']})
            
        #end-for-i
    #end-for-x
    
    print('arr = {}'.format( str(len(arr)) ))
    print('*** py end ***')
    return 0
#end-main

## flow
main()


