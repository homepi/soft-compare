
## dependencies
import csv

## config
FILE_NAME='../../data/products.csv'
data = []

## functions
def init():
	
	with open(FILE_NAME, newline='') as csvfile:
		reader = csv.DictReader(csvfile, delimiter=';')
		for row in reader:
			obj = row
			obj["ID"] = int(obj["ID"])
			obj["PRICE"] = float(obj["PRICE"])
			obj["STOCK"] = int(obj["STOCK"])
			data.append(obj)
			
	print('dba.init - sample =', data[0])
	
#end-init

def search(query):
	keys = query.keys()
	keyLen = len(keys)
	
	if keyLen == 0:
		return data
		
	
	result = []
	
	for x in data:
		match = []
		
		for k in keys:
			keyUp = k.upper()
			if type(query[k]) == int:
				if( x[keyUp] == query[k] ):
					match.append(True)
			else: 
				if( x[keyUp].lower() == query[k].lower() ):
					match.append(True)
			
		if( len(match) == keyLen ):
			result.append(x)
		
	return result
#end search

def update(obj):
	return True
#end update


