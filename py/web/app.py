
## dependencies
import os
from flask import Flask
from flask import request

import lib.dba as dba

## config
port = int(os.getenv('PORT', 9000))
app = Flask(__name__)

## functions

## routing
@app.route("/")
def hello():
	return { "test": True, "app": "py" }
	
@app.route("/api/products")
def search():
	data = dba.search(request.args)
	return { "data": data }
	
@app.route("/api/products/<int:id>/inc", methods = ['POST'])
def increment(id):
	amount = request.json["amount"] if type(request.json["amount"]) == int else 1
	
	result = dba.search({"id": id})
	print(result)
	found = result[0]
	found["STOCK"] += amount
	
	dba.update(found)
	return found

## flow
if __name__ == '__main__':
	dba.init()
	app.run(host="0.0.0.0", threaded=True, port=port)
	#app.logger.info('%s logged in successfully', user.username)


