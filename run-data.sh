
set -x;

#/usr/bin/time

\time -f "mem=%K RSS=%M elapsed=%E cpu.sys=%S user=%U" node js/data.js

\time -f "mem=%K RSS=%M elapsed=%E cpu.sys=%S user=%U" python py/data.py

# cd java && \time -f "mem=%K RSS=%M elapsed=%E cpu.sys=%S user=%U" env MAVEN_OPTS="-Xms2g -Xmx8g" mvn exec:java && cd ..

\time -f "mem=%K RSS=%M elapsed=%E cpu.sys=%S user=%U" go run go/data.go

\time -f "mem=%K RSS=%M elapsed=%E cpu.sys=%S user=%U" ruby ruby/data.rb


