package main

import (
    "fmt"
	"encoding/json"
	"io/ioutil"
)

func main() {
    
	fmt.Println("*** golang - start ***");
	content, _ := ioutil.ReadFile("./data/large-json.json")
	
	var result []map[string]interface{}
    json.Unmarshal([]byte(content), &result)
	
	fmt.Println("len = ", len(result))
	var arr []map[string]interface{}
	
	for _, x := range result {
		
		var ind = x["index"].(float64)
		var age = x["age"].(float64)
		var sum = int(ind) + int(age) + len( x["friends"].([]interface{}) )
		var prod = sum * 16782;
		var mod = prod % 15000;
		
		for i := 0; i < mod; i++ {
			obj := make(map[string]interface{})
			obj["sum"] = sum
			obj["prod"] = prod
			obj["mod"] = mod
			obj["i"] = i
			obj["id"] = x["id"]
			
			arr = append(arr, obj)
		}
		
	}
	
	fmt.Println("arr - ", len(arr))
	fmt.Println("*** golang - end ***")
}


