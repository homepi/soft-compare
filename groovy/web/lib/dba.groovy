package lib

// ## dependencies
import groovy.util.logging.Slf4j;
@Grab('com.opencsv:opencsv:5.3')
import com.opencsv.bean.CsvToBeanBuilder

import lib.product as Product

@Slf4j
class dba {
	
	String FILE_NAME = "../../data/products.csv"
	List<Product> DataHolder;
	
	dba(){
		log.info('dba - constructor...')
		
	}
	public void init(){
		log.info('dba - init...')
		
		def fileReader = new FileReader(FILE_NAME)
		def csvBuilder = new CsvToBeanBuilder(fileReader)
		def csvOpts = csvBuilder.withSeparator((char)';')
			.withSkipLines(1)
			.withIgnoreLeadingWhiteSpace(true)
			.withFilter(stringValues -> Arrays.stream(stringValues)
				.anyMatch(value -> value != null && value.length() > 0))
			.withType(Product.class);
		
		DataHolder = csvOpts.build().parse()
		//DataHolder.forEach(x -> log.info('loaded - id = {}', x.Id));
		
	}
	
	public List<Product> Search(Map query){
		
		def keyLen = query.size()
		log.info('Search - start - keyLen = {}', keyLen)
		
		def result = DataHolder.findAll( x -> {
			def hit = []
			
			query.each { key, val ->
				def objKey = key.toUpperCase()
				def objVal = x[objKey]
				def check = false
				log.info('k = {} | q = {} | val = {}', key, val, objVal)
				
				if( objVal instanceof String ){
					check = val.equalsIgnoreCase(objVal)
				}
				log.info('DEBUG = {} | {}', objVal.getClass(), val.getClass() )
				if( objVal instanceof Integer ){
					check = ( objVal == (int)val )
				}
				hit << check
			}
			
			return hit.grep(t->t).size() == keyLen
		})
		
		return result;
	}
	
	public boolean Update(Product obj){
		return true
	}
}


