package lib

@Grab('com.opencsv:opencsv:5.3')
import com.opencsv.bean.CsvBindByPosition

public class product {
	@CsvBindByPosition(position = 0)
	public int ID;
	
	@CsvBindByPosition(position = 1)
	public String CATEGORY;
	
	@CsvBindByPosition(position = 2)
	public String NAME;
	
	@CsvBindByPosition(position = 3)
	public double PRICE;
	
	@CsvBindByPosition(position = 4)
	public int STOCK;
}


