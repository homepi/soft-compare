
// ## dependencies
import groovy.util.logging.Slf4j;

import lib.dba as Dba;
import lib.product as Product;

//https://stackoverflow.com/questions/2621180/groovy-load-csv-files

@Controller
@Slf4j
class App {
	
	Dba db;
	
	public App(){
		
		log.info("Started...")
		db = new Dba()
		db.init()
	}
	
	@RequestMapping(
		value = "/", 
		method = RequestMethod.GET, 
		produces="application/json"
	) 
	@ResponseBody
	public String hello(){
		log.info("GET / - start...")
		return "{\"test\": true, \"app\": \"groovy\" }"
	}
	
	@RequestMapping(
		value = "/api/products", 
		method = RequestMethod.GET, 
		produces="application/json"
	) 
	@ResponseBody
	public Map<String, Object> search(@RequestParam(required=false) Map<String, String> qparams){
		log.info("GET /api/products - start - q = {}", qparams.toString())
		def data = db.Search( qparams )
		return [data: data]
	}
	
	@RequestMapping(
		value = "/api/products/{id}/inc",
		method = RequestMethod.POST,
		produces="application/json"
	) 
	@ResponseBody
	public Product update(@PathVariable int id, @RequestBody Map body)
	{
		
		log.info("POST - start - id = {}", id)
		
		def query = [id: id]
		def obj = db.Search( query )[0]
		log.info("POST - found - name = {} | stock = {}", obj.NAME, obj.STOCK)
		log.info("DEBUG = {}", obj.STOCK.getClass())
		
		int toadd = 1
		if( body.amount && body.amount instanceof Integer ){
			toadd = body.amount
		}
		obj.STOCK += toadd
		db.Update(obj)
		
		return obj
	}
}