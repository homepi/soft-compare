
java_folder=java/web/src/main/java/tw/javaweb
java_ext=java
java_files=( Application Controller Dba Product)

py_folder=py/web
py_ext=py
py_files=( app lib/dba )

groovy_folder=groovy/web
groovy_ext=groovy
groovy_files=( app lib/dba lib/product )

js_folder=js/web
js_ext=js
js_files=( app routes/product lib/dba lib/log )

array=( "${js_files[@]}" )
ext=$js_ext
target_folder=$js_folder
tot=0

for i in "${array[@]}"
do
	fp="${target_folder}/${i}.${ext}"
	cnt=$(cat $fp |tr -d ' ' |tr -d "\n" |tr -d "\t" | wc -c)
	echo "$i = $cnt"
	tot=$((tot+cnt))
done

echo "tot $ext = $tot"


