
## dep
require 'json'

## config
$filePath = './data/large-json.json'

## fund
def main
	puts("*** ruby start ***")
	file = File.read($filePath)
	data = JSON.parse(file)
	
	puts("len = %d" % data.length )
	arr = []
	
	data.each do |x|
		
		sum = x['index'] + x["age"] + x["friends"].length
		prod = sum * 16782;
		mod = prod % 15000;
		
		for i in 0..mod do
			arr << {"sum" => sum, "prod" => prod, "mod" => mod, "i" => i, "id" => x['_id']}
		end
	end
	
	puts('arr = %d' % arr.size)
	puts('*** ruby end ***')
	return 0
	
end

## flow
main()


